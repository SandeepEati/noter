﻿using Noter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noter.ViewModels
{
    public class NoterVM : BaseVM
    {
        private List<Note> _Notes = new List<Note>();

        public List<Note> Notes
        {
            get
            {
                return _Notes;
            }

            set
            {
                if (_Notes != value)
                {
                    _Notes = value;
                    RaisePropertyChanged("Notes");
                }
            }
        }

        public NoterVM()
        {
            Notes = new List<Note>()
            {
                new Note() { Title = "Artificial Intelligence" },
                new Note() { Title = "Machine Learning" },
                new Note() { Title = "Azure" },
                new Note() { Title = "Blockchain" },
                new Note() { Title = "Azure" },
                new Note() { Title = "AWS" }
            };
        }
    }
}
