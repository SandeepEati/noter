﻿using Noter.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Recognition;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Noter.Views
{
    /// <summary>
    /// Interaction logic for NoterV.xaml
    /// </summary>
    public partial class NoterV : Window
    {
        public NoterV()
        {
            InitializeComponent();
            this.DataContext = new NoterVM();
            //SpeechRecognizer speechRecognizer = new SpeechRecognizer();
        }
    }
}
